| feature name | description |
| ------ | ------ |
|ACC1_SampleUS|time since system startup this sample was taken|
|ACC1_AccX|acceleration along X axis|
|ACC1_AccY|acceleration along Y axis|
|ACC1_AccZ|acceleration along Z axis|
|ACC2_SampleUS|time since system startup this sample was taken|
|ACC2_AccX|acceleration along X axis|
|ACC2_AccY|acceleration along Y axis|
|ACC2_AccZ|acceleration along Z axis|
|ACC3_SampleUS|time since system startup this sample was taken|
|ACC3_AccX|acceleration along X axis|
|ACC3_AccY|acceleration along Y axis|
|ACC3_AccZ|acceleration along Z axis|
|ADSB_ICAO_address|Transponder address|
|ADSB_Lat|Vehicle latitude|
|ADSB_Lng|Vehicle longitude|
|ADSB_Alt|Vehicle altitude|
|ADSB_Heading|Vehicle heading|
|ADSB_Hor_vel|Vehicle horizontal velocity|
|ADSB_Ver_vel|Vehicle vertical velocity|
|ADSB_Squark|Transponder squawk code|
|AETR_Ail|Pre-mixer value for aileron output (between -4500 to 4500)|
|AETR_Elev|Pre-mixer value for elevator output (between -4500 to 4500)|
|AETR_Thr|Pre-mixer value for throttle output (between -4500 to 4500)|
|AETR_Rudd|Pre-mixer value for rudder output (between -4500 to 4500)|
|AETR_Flap|Pre-mixer value for flaps output (between -4500 to 4500)|
|AETR_SS|Surface movement / airspeed scaling value|
|AHR2_Roll|Estimated roll|
|AHR2_Pitch|Estimated pitch|
|AHR2_Yaw|Estimated yaw|
|AHR2_Alt|Estimated altitude|
|AHR2_Lat|Estimated latitude|
|AHR2_Lng|Estimated longitude|
|AHR2_Q1|Estimated attitude quaternion component 1|
|AHR2_Q2|Estimated attitude quaternion component 2|
|AHR2_Q3|Estimated attitude quaternion component 3|
|AHR2_Q4|Estimated attitude quaternion component 4|
|AOA_AOA|Angle of Attack calculated from airspeed, wind vector,velocity vector|
|AOA_SSA|Side Slip Angle calculated from airspeed, wind vector,velocity vector|
|ARM_ArmState|true if vehicle is now armed|
|ARM_ArmChecks|arming bitmask at time of arming|
|ARM_Forced|true if arm/disarm was forced|
|ARM_Method|method used for arming|
|AROT_P|P-term for headspeed controller response|
|AROT_hserr|head speed error; difference between current and desired head speed|
|AROT_ColOut|Collective Out|
|AROT_FFCol|FF-term for headspeed controller response|
|AROT_CRPM|current headspeed RPM|
|AROT_SpdF|current forward speed|
|AROT_CmdV|desired forward speed|
|AROT_p|p-term of velocity response|
|AROT_ff|ff-term of velocity response|
|AROT_AccO|forward acceleration output|
|AROT_AccT|forward acceleration target|
|AROT_PitT|pitch target|
|ARSP_Airspeed|Current airspeed|
|ARSP_DiffPress|Pressure difference between static and dynamic port|
|ARSP_Temp|Temperature used for calculation|
|ARSP_RawPress|Raw pressure less offset|
|ARSP_Offset|Offset from parameter|
|ARSP_U|True if sensor is being used|
|ARSP_Health|True if sensor is healthy|
|ARSP_Hfp|Probability sensor has failed|
|ARSP_Pri|True if sensor is the primary sensor|
|ASM1_TUS|Simulation’s timestamp|
|ASM1_R|Simulation’s roll|
|ASM1_P|Simulation’s pitch|
|ASM1_Y|Simulation’s yaw|
|ASM1_GX|Simulated gyroscope, X-axis|
|ASM1_GY|Simulated gyroscope, Y-axis|
|ASM1_GZ|Simulated gyroscope, Z-axis|
|ASM2_AX|simulation’s acceleration, X-axis|
|ASM2_AY|simulation’s acceleration, Y-axis|
|ASM2_AZ|simulation’s acceleration, Z-axis|
|ASM2_VX|simulation’s velocity, X-axis|
|ASM2_VY|simulation’s velocity, Y-axis|
|ASM2_VZ|simulation’s velocity, Z-axis|
|ASM2_PX|simulation’s position, X-axis|
|ASM2_PY|simulation’s position, Y-axis|
|ASM2_PZ|simulation’s position, Z-axis|
|ASM2_Alt|simulation’s gps altitude|
|ASM2_SD|simulation’s earth-frame speed-down|
|ASP2_Airspeed|Current airspeed|
|ASP2_DiffPress|Pressure difference between static and dynamic port|
|ASP2_Temp|Temperature used for calculation|
|ASP2_RawPress|Raw pressure less offset|
|ASP2_Offset|Offset from parameter|
|ASP2_U|True if sensor is being used|
|ASP2_Health|True if sensor is healthy|
|ASP2_Hfp|Probability sensor has failed|
|ASP2_Pri|True if sensor is the primary sensor|
|ATDE_Angle|current angle|
|ATDE_Rate|current angular rate|
|ATRP_Type|Type of autotune (0 = Roll/ 1 = Pitch)|
|ATRP_State|AutoTune state|
|ATRP_Servo|Normalised control surface output (between -4500 to 4500)|
|ATRP_Demanded|Desired Pitch/Roll rate|
|ATRP_Achieved|Achieved Pitch/Roll rate|
|ATRP_P|Proportional part of PID|
|ATT_DesRoll|vehicle desired roll|
|ATT_Roll|achieved vehicle roll|
|ATT_DesPitch|vehicle desired pitch|
|ATT_Pitch|achieved vehicle pitch|
|ATT_DesYaw|vehicle desired yaw|
|ATT_Yaw|achieved vehicle yaw|
|ATT_ErrRP|lowest estimated gyro drift error|
|ATT_ErrYaw|difference between measured yaw and DCM yaw estimate|
|ATUN_Axis|which axis is currently being tuned|
|ATUN_TuneStep|step in autotune process|
|ATUN_Targ|target angle or rate, depending on tuning step|
|ATUN_Min|measured minimum target angle or rate|
|ATUN_Max|measured maximum target angle or rate|
|ATUN_RP|new rate gain P term|
|ATUN_RD|new rate gain D term|
|ATUN_SP|new angle P term|
|ATUN_ddt|maximum measured twitching acceleration|
|BAR2_Alt|calculated altitude|
|BAR2_Press|measured atmospheric pressure|
|BAR2_Temp|measured atmospheric temperature|
|BAR2_CRt|derived climb rate from primary barometer|
|BAR2_SMS|time last sample was taken|
|BAR2_Offset|raw adjustment of barometer altitude, zeroed on calibration, possibly set by GCS|
|BAR2_GndTemp|temperature on ground, specified by parameter or measured while on ground|
|BAR2_Health|true if barometer is considered healthy|
|BAR3_Alt|calculated altitude|
|BAR3_Press|measured atmospheric pressure|
|BAR3_Temp|measured atmospheric temperature|
|BAR3_CRt|derived climb rate from primary barometer|
|BAR3_SMS|time last sample was taken|
|BAR3_Offset|raw adjustment of barometer altitude, zeroed on calibration, possibly set by GCS|
|BAR3_GndTemp|temperature on ground, specified by parameter or measured while on ground|
|BAR3_Health|true if barometer is considered healthy|
|BARO_Alt|calculated altitude|
|BARO_Press|measured atmospheric pressure|
|BARO_Temp|measured atmospheric temperature|
|BARO_CRt|derived climb rate from primary barometer|
|BARO_SMS|time last sample was taken|
|BARO_Offset|raw adjustment of barometer altitude, zeroed on calibration, possibly set by GCS|
|BARO_GndTemp|temperature on ground, specified by parameter or measured while on ground|
|BARO_Health|true if barometer is considered healthy|
|BAT_Instance|battery instance number|
|BAT_Volt|measured voltage|
|BAT_VoltR|estimated resting voltage|
|BAT_Curr|measured current|
|BAT_CurrTot|current * time|
|BAT_EnrgTot|energy this battery has produced|
|BAT_Temp|measured temperature|
|BAT_Res|estimated battery resistance|
|BCL_Instance|battery instance number|
|BCL_Volt|battery voltage|
|BCL_V1|first cell voltage|
|BCL_V2|second cell voltage|
|BCL_V3|third cell voltage|
|BCL_V4|fourth cell voltage|
|BCL_V5|fifth cell voltage|
|BCL_V6|sixth cell voltage|
|BCL_V7|seventh cell voltage|
|BCL_V8|eighth cell voltage|
|BCL_V9|ninth cell voltage|
|BCL_V10|tenth cell voltage|
|BCL_V11|eleventh cell voltage|
|BCL_V12|twelfth cell voltage|
|BCN_Health|True if beacon sensor is healthy|
|BCN_Cnt|Number of beacons being used|
|BCN_D0|Distance to first beacon|
|BCN_D1|Distance to second beacon|
|BCN_D2|Distance to third beacon|
|BCN_D3|Distance to fouth beacon|
|BCN_PosX|Calculated beacon position, x-axis|
|BCN_PosY|Calculated beacon position, y-axis|
|BCN_PosZ|Calculated beacon position, z-axis|
|CAM_GPSTime|milliseconds since start of GPS week|
|CAM_GPSWeek|weeks since 5 Jan 1980|
|CAM_Lat|current latitude|
|CAM_Lng|current longitude|
|CAM_Alt|current altitude|
|CAM_RelAlt|current altitude relative to home|
|CAM_GPSAlt|altitude as reported by GPS|
|CAM_Roll|current vehicle roll|
|CAM_Pitch|current vehicle pitch|
|CAM_Yaw|current vehicle yaw|
|CESC_Id|ESC identifier|
|CESC_ECnt|Error count|
|CESC_Voltage|Battery voltage measurement|
|CESC_Curr|Battery current measurement|
|CESC_Temp|Temperature|
|CESC_RPM|Measured RPM|
|CESC_Pow|Rated power output|
|CMD_CTot|Total number of mission commands|
|CMD_CNum|This command’s offset in mission|
|CMD_CId|Command type|
|CMD_Prm1|Parameter 1|
|CMD_Prm2|Parameter 2|
|CMD_Prm3|Parameter 3|
|CMD_Prm4|Parameter 4|
|CMD_Lat|Command latitude|
|CMD_Lng|Command longitude|
|CMD_Alt|Command altitude|
|CMD_Frame|Frame used for position|
|CMDI_CId|command id|
|CMDI_TSys|target system|
|CMDI_TCmp|target component|
|CMDI_cur|current|
|CMDI_cont|autocontinue|
|CMDI_Prm1|parameter 1|
|CMDI_Prm2|parameter 2|
|CMDI_Prm3|parameter 3|
|CMDI_Prm4|parameter 4|
|CMDI_Lat|target latitude|
|CMDI_Lng|target longitude|
|CMDI_Alt|target altitude|
|CMDI_F|frame|
|COFS_OfsX|best learnt offset, x-axis|
|COFS_OfsY|best learnt offset, y-axis|
|COFS_OfsZ|best learnt offset, z-axis|
|COFS_Var|error of best offset vector|
|COFS_Yaw|best learnt yaw|
|COFS_WVar|error of best learn yaw|
|COFS_N|number of samples used|
|CSRV_Id|Servo number this data relates to|
|CSRV_Pos|Current servo position|
|CSRV_Force|Force being applied|
|CSRV_Speed|Current servo movement speed|
|CSRV_Pow|Amount of rated power being applied|
|CTRL_RMSRollP|LPF Root-Mean-Squared Roll Rate controller P gain|
|CTRL_RMSRollD|LPF Root-Mean-Squared Roll rate controller D gain|
|CTRL_RMSPitchP|LPF Root-Mean-Squared Pitch Rate controller P gain|
|CTRL_RMSPitchD|LPF Root-Mean-Squared Pitch Rate controller D gain|
|CTRL_RMSYaw|LPF Root-Mean-Squared Yaw Rate controller P+D gain|
|CTUN_NavRoll|desired roll|
|CTUN_Roll|achieved roll|
|CTUN_NavPitch|desired pitch|
|CTUN_Pitch|achieved pitch|
|CTUN_ThrOut|scaled output throttle|
|CTUN_RdrOut|scaled output rudder|
|CTUN_ThrDem|demanded speed-height-controller throttle|
|CTUN_Aspd|airspeed estimate (or measurement if airspeed sensor healthy and ARSPD_USE>0)|
|CTUN_SAs|synthetic airspeed measurement derived from non-airspeed sensors, NaN if not available|
|CTUN_E2T|equivalent to true airspeed ratio|
|DMS_N|Current block number|
|DMS_Dp|Number of times we rejected a write to the backend|
|DMS_RT|Number of blocks sent from the retry queue|
|DMS_RS|Number of resends of unacknowledged data made|
|DMS_Fa|Average number of blocks on the free list|
|DMS_Fmn|Minimum number of blocks on the free list|
|DMS_Fmx|Maximum number of blocks on the free list|
|DMS_Pa|Average number of blocks on the pending list|
|DMS_Pmn|Minimum number of blocks on the pending list|
|DMS_Pmx|Maximum number of blocks on the pending list|
|DMS_Sa|Average number of blocks on the sent list|
|DMS_Smn|Minimum number of blocks on the sent list|
|DMS_Smx|Maximum number of blocks on the sent list|
|DSF_Dp|Number of times we rejected a write to the backend|
|DSF_Blk|Current block number|
|DSF_Bytes|Current write offset|
|DSF_FMn|Minimum free space in write buffer in last time period|
|DSF_FMx|Maximum free space in write buffer in last time period|
|DSF_FAv|Average free space in write buffer in last time period|
|DSTL_Stg|Deepstall landing stage|
|DSTL_THdg|Target heading|
|DSTL_Lat|Landing point latitude|
|DSTL_Lng|Landing point longitude|
|DSTL_Alt|Landing point altitude|
|DSTL_XT|Crosstrack error|
|DSTL_Travel|Expected travel distance vehicle will travel from this point|
|DSTL_L1I|L1 controller crosstrack integrator value|
|DSTL_Loiter|wind estimate loiter angle flown|
|DSTL_Des|Deepstall steering PID desired value|
|DSTL_P|Deepstall steering PID Proportional response component|
|DSTL_I|Deepstall steering PID Integral response component|
|DSTL_D|Deepstall steering PID Derivative response component|
|ECYL_Inst|Cylinder this data belongs to|
|ECYL_IgnT|Ignition timing|
|ECYL_InjT|Injection time|
|ECYL_CHT|Cylinder head temperature|
|ECYL_EGT|Exhaust gas temperature|
|ECYL_Lambda|Estimated lambda coefficient (dimensionless ratio)|
|ECYL_IDX|Index of the publishing ECU|
|EFI_LP|Reported engine load|
|EFI_Rpm|Reported engine RPM|
|EFI_SDT|Spark Dwell Time|
|EFI_ATM|Atmospheric pressure|
|EFI_IMP|Intake manifold pressure|
|EFI_IMT|Intake manifold temperature|
|EFI_ECT|Engine Coolant Temperature|
|EFI_OilP|Oil Pressure|
|EFI_OilT|Oil temperature|
|EFI_FP|Fuel Pressure|
|EFI_FCR|Fuel Consumption Rate|
|EFI_CFV|Consumed fueld volume|
|EFI_TPS|Throttle Position|
|EFI_IDX|Index of the publishing ECU|
|EFI2_Healthy|True if EFI is healthy|
|EFI2_ES|Engine state|
|EFI2_GE|General error|
|EFI2_CSE|Crankshaft sensor status|
|EFI2_TS|Temperature status|
|EFI2_FPS|Fuel pressure status|
|EFI2_OPS|Oil pressure status|
|EFI2_DS|Detonation status|
|EFI2_MS|Misfire status|
|EFI2_DebS|Debris status|
|EFI2_SPU|Spark plug usage|
|EFI2_IDX|Index of the publishing ECU|
|ERR_Subsys|Subsystem in which the error occurred|
|ERR_ECode|Subsystem-specific error code|
|ESC_Instance|ESC instance number|
|ESC_RPM|reported motor rotation rate|
|ESC_Volt|Perceived input voltage for the ESC|
|ESC_Curr|Perceived current through the ESC|
|ESC_Temp|ESC temperature|
|ESC_CTot|current consumed total|
|ESC_MotTemp|measured motor temperature|
|EV_Id|Event identifier|
|FMT_Type|unique-to-this-log identifier for message being defined|
|FMT_Length|the number of bytes taken up by this message (including all headers)|
|FMT_Name|name of the message being defined|
|FMT_Format|character string defining the C-storage-type of the fields in this message|
|FMT_Columns|the labels of the message being defined|
|FMTU_FmtType|numeric reference to associated FMT message|
|FMTU_UnitIds|each character refers to a UNIT message.  The unit at an offset corresponds to the field at the same offset in FMT.Format|
|FMTU_MultIds|each character refers to a MULT message.  The multiplier at an offset corresponds to the field at the same offset in FMT.Format|
|FOLL_Lat|Target latitude|
|FOLL_Lon|Target longitude|
|FOLL_Alt|Target absolute altitude|
|FOLL_VelN|Target earth-frame velocity, North|
|FOLL_VelE|Target earth-frame velocity, East|
|FOLL_VelD|Target earth-frame velocity, Down|
|FOLL_LatE|Vehicle latitude|
|FOLL_LonE|Vehicle longitude|
|FOLL_AltE|Vehicle absolute altitude|
|FTN_NDn|number of active dynamic harmonic notches|
|FTN_DnF1|dynamic harmonic notch centre frequency for motor 1|
|FTN_DnF2|dynamic harmonic notch centre frequency for motor 2|
|FTN_DnF3|dynamic harmonic notch centre frequency for motor 3|
|FTN_DnF4|dynamic harmonic notch centre frequency for motor 4|
|FTN1_PkAvg|peak noise frequency as an energy-weighted average of roll and pitch peak frequencies|
|FTN1_BwAvg|bandwidth of weighted peak freqency where edges are determined by FFT_ATT_REF|
|FTN1_DnF|dynamic harmonic notch centre frequency|
|FTN1_SnX|signal-to-noise ratio on the roll axis|
|FTN1_SnY|signal-to-noise ratio on the pitch axis|
|FTN1_SnZ|signal-to-noise ratio on the yaw axis|
|FTN1_FtX|harmonic fit on roll of the highest noise peak to the second highest noise peak|
|FTN1_FtY|harmonic fit on pitch of the highest noise peak to the second highest noise peak|
|FTN1_FtZ|harmonic fit on yaw of the highest noise peak to the second highest noise peak|
|FTN1_FH|FFT health|
|FTN1_Tc|FFT cycle time|
|FTN2_Id|peak id where 0 is the centre peak, 1 is the lower shoulder and 2 is the upper shoulder|
|FTN2_PkX|noise frequency of the peak on roll|
|FTN2_PkY|noise frequency of the peak on pitch|
|FTN2_PkZ|noise frequency of the peak on yaw|
|FTN2_DnF|dynamic harmonic notch centre frequency|
|FTN2_BwX|bandwidth of the peak freqency on roll where edges are determined by FFT_ATT_REF|
|FTN2_BwY|bandwidth of the peak freqency on pitch where edges are determined by FFT_ATT_REF|
|FTN2_BwZ|bandwidth of the peak freqency on yaw where edges are determined by FFT_ATT_REF|
|FTN2_EnX|power spectral density bin energy of the peak on roll|
|FTN2_EnY|power spectral density bin energy of the peak on roll|
|FTN2_EnZ|power spectral density bin energy of the peak on roll|
|GPA_VDop|vertical degree of procession|
|GPA_HAcc|horizontal position accuracy|
|GPA_VAcc|vertical position accuracy|
|GPA_SAcc|speed accuracy|
|GPA_YAcc|yaw accuracy|
|GPA_VV|true if vertical velocity is available|
|GPA_SMS|time since system startup this sample was taken|
|GPA_Delta|system time delta between the last two reported positions|
|GPA2_VDop|vertical degree of procession|
|GPA2_HAcc|horizontal position accuracy|
|GPA2_VAcc|vertical position accuracy|
|GPA2_SAcc|speed accuracy|
|GPA2_YAcc|yaw accuracy|
|GPA2_VV|true if vertical velocity is available|
|GPA2_SMS|time since system startup this sample was taken|
|GPA2_Delta|system time delta between the last two reported positions|
|GPAB_VDop|vertical degree of procession|
|GPAB_HAcc|horizontal position accuracy|
|GPAB_VAcc|vertical position accuracy|
|GPAB_SAcc|speed accuracy|
|GPAB_YAcc|yaw accuracy|
|GPAB_VV|true if vertical velocity is available|
|GPAB_SMS|time since system startup this sample was taken|
|GPAB_Delta|system time delta between the last two reported positions|
|GPS_Status|GPS Fix type; 2D fix, 3D fix etc.|
|GPS_GMS|milliseconds since start of GPS Week|
|GPS_GWk|weeks since 5 Jan 1980|
|GPS_NSats|number of satellites visible|
|GPS_HDop|horizontal precision|
|GPS_Lat|latitude|
|GPS_Lng|longitude|
|GPS_Alt|altitude|
|GPS_Spd|ground speed|
|GPS_GCrs|ground course|
|GPS_VZ|vertical speed|
|GPS_Yaw|vehicle yaw|
|GPS_U|boolean value indicating whether this GPS is in use|
|GPS2_Status|GPS Fix type; 2D fix, 3D fix etc.|
|GPS2_GMS|milliseconds since start of GPS Week|
|GPS2_GWk|weeks since 5 Jan 1980|
|GPS2_NSats|number of satellites visible|
|GPS2_HDop|horizontal precision|
|GPS2_Lat|latitude|
|GPS2_Lng|longitude|
|GPS2_Alt|altitude|
|GPS2_Spd|ground speed|
|GPS2_GCrs|ground course|
|GPS2_VZ|vertical speed|
|GPS2_Yaw|vehicle yaw|
|GPS2_U|boolean value indicating whether this GPS is in use|
|GPSB_Status|GPS Fix type; 2D fix, 3D fix etc.|
|GPSB_GMS|milliseconds since start of GPS Week|
|GPSB_GWk|weeks since 5 Jan 1980|
|GPSB_NSats|number of satellites visible|
|GPSB_HDop|horizontal precision|
|GPSB_Lat|latitude|
|GPSB_Lng|longitude|
|GPSB_Alt|altitude|
|GPSB_Spd|ground speed|
|GPSB_GCrs|ground course|
|GPSB_VZ|vertical speed|
|GPSB_Yaw|vehicle yaw|
|GPSB_U|boolean value indicating whether this GPS is in use|
|GRAW_WkMS|receiver TimeOfWeek measurement|
|GRAW_Week|GPS week|
|GRAW_numSV|number of space vehicles seen|
|GRAW_sv|space vehicle number of first vehicle|
|GRAW_cpMes|carrier phase measurement|
|GRAW_prMes|pseudorange measurement|
|GRAW_doMes|Doppler measurement|
|GRAW_mesQI|measurement quality index|
|GRAW_cno|carrier-to-noise density ratio|
|GRAW_lli|loss of lock indicator|
|GRXH_rcvTime|receiver TimeOfWeek measurement|
|GRXH_week|GPS week|
|GRXH_leapS|GPS leap seconds|
|GRXH_numMeas|number of space-vehicle measurements to follow|
|GRXH_recStat|receiver tracking status bitfield|
|GRXS_prMes|Pseudorange measurement|
|GRXS_cpMes|Carrier phase measurement|
|GRXS_doMes|Doppler measurement|
|GRXS_gnss|GNSS identifier|
|GRXS_sv|Satellite identifier|
|GRXS_freq|GLONASS frequency slot|
|GRXS_lock|carrier phase locktime counter|
|GRXS_cno|carrier-to-noise density ratio|
|GRXS_prD|estimated pseudorange measurement standard deviation|
|GRXS_cpD|estimated carrier phase measurement standard deviation|
|GRXS_doD|estimated Doppler measurement standard deviation|
|GRXS_trk|tracking status bitfield|
|GYR1_SampleUS|time since system startup this sample was taken|
|GYR1_GyrX|measured rotation rate about X axis|
|GYR1_GyrY|measured rotation rate about Y axis|
|GYR1_GyrZ|measured rotation rate about Z axis|
|GYR2_SampleUS|time since system startup this sample was taken|
|GYR2_GyrX|measured rotation rate about X axis|
|GYR2_GyrY|measured rotation rate about Y axis|
|GYR2_GyrZ|measured rotation rate about Z axis|
|GYR3_SampleUS|time since system startup this sample was taken|
|GYR3_GyrX|measured rotation rate about X axis|
|GYR3_GyrY|measured rotation rate about Y axis|
|GYR3_GyrZ|measured rotation rate about Z axis|
|HEAT_Temp|Current IMU temperature|
|HEAT_Targ|Target IMU temperature|
|HEAT_P|Proportional portion of response|
|HEAT_I|Integral portion of response|
|HEAT_Out|Controller output to heating element|
|ICMB_Traw|raw temperature from sensor|
|ICMB_Praw|raw pressure from sensor|
|ICMB_P|pressure|
|ICMB_T|temperature|
|IMT_DelT|Delta time|
|IMT_DelvT|Delta velocity accumulation time|
|IMT_DelaT|Delta angle accumulation time|
|IMT_DelAX|Accumulated delta angle X|
|IMT_DelAY|Accumulated delta angle Y|
|IMT_DelAZ|Accumulated delta angle Z|
|IMT_DelVX|Accumulated delta velocity X|
|IMT_DelVY|Accumulated delta velocity Y|
|IMT_DelVZ|Accumulated delta velocity Z|
|IMT2_DelT|Delta time|
|IMT2_DelvT|Delta velocity accumulation time|
|IMT2_DelaT|Delta angle accumulation time|
|IMT2_DelAX|Accumulated delta angle X|
|IMT2_DelAY|Accumulated delta angle Y|
|IMT2_DelAZ|Accumulated delta angle Z|
|IMT2_DelVX|Accumulated delta velocity X|
|IMT2_DelVY|Accumulated delta velocity Y|
|IMT2_DelVZ|Accumulated delta velocity Z|
|IMT3_DelT|Delta time|
|IMT3_DelvT|Delta velocity accumulation time|
|IMT3_DelaT|Delta angle accumulation time|
|IMT3_DelAX|Accumulated delta angle X|
|IMT3_DelAY|Accumulated delta angle Y|
|IMT3_DelAZ|Accumulated delta angle Z|
|IMT3_DelVX|Accumulated delta velocity X|
|IMT3_DelVY|Accumulated delta velocity Y|
|IMT3_DelVZ|Accumulated delta velocity Z|
|IMU_GyrX|measured rotation rate about X axis|
|IMU_GyrY|measured rotation rate about Y axis|
|IMU_GyrZ|measured rotation rate about Z axis|
|IMU_AccX|acceleration along X axis|
|IMU_AccY|acceleration along Y axis|
|IMU_AccZ|acceleration along Z axis|
|IMU_EG|gyroscope error count|
|IMU_EA|accelerometer error count|
|IMU_T|IMU temperature|
|IMU_GH|gyroscope health|
|IMU_AH|accelerometer health|
|IMU_GHz|gyroscope measurement rate|
|IMU_AHz|accelerometer measurement rate|
|IMU2_GyrX|measured rotation rate about X axis|
|IMU2_GyrY|measured rotation rate about Y axis|
|IMU2_GyrZ|measured rotation rate about Z axis|
|IMU2_AccX|acceleration along X axis|
|IMU2_AccY|acceleration along Y axis|
|IMU2_AccZ|acceleration along Z axis|
|IMU2_EG|gyroscope error count|
|IMU2_EA|accelerometer error count|
|IMU2_T|IMU temperature|
|IMU2_GH|gyroscope health|
|IMU2_AH|accelerometer health|
|IMU2_GHz|gyroscope measurement rate|
|IMU2_AHz|accelerometer measurement rate|
|IMU3_GyrX|measured rotation rate about X axis|
|IMU3_GyrY|measured rotation rate about Y axis|
|IMU3_GyrZ|measured rotation rate about Z axis|
|IMU3_AccX|acceleration along X axis|
|IMU3_AccY|acceleration along Y axis|
|IMU3_AccZ|acceleration along Z axis|
|IMU3_EG|gyroscope error count|
|IMU3_EA|accelerometer error count|
|IMU3_T|IMU temperature|
|IMU3_GH|gyroscope health|
|IMU3_AH|accelerometer health|
|IMU3_GHz|gyroscope measurement rate|
|IMU3_AHz|accelerometer measurement rate|
|IOMC_Mem|Free memory|
|IOMC_TS|IOMCU uptime|
|IOMC_NPkt|Number of packets received by IOMCU|
|IOMC_Nerr|Protocol failures on MCU side|
|IOMC_Nerr2|Reported number of failures on IOMCU side|
|IOMC_NDel|Number of delayed packets received by MCU|
|JSN1_TStamp|Simulation’s timestamp (s)|
|JSN1_R|Simulation’s roll (rad)|
|JSN1_P|Simulation’s pitch (rad)|
|JSN1_Y|Simulation’s yaw (rad)|
|JSN1_GX|Simulated gyroscope, X-axis (rad/sec)|
|JSN1_GY|Simulated gyroscope, Y-axis (rad/sec)|
|JSN1_GZ|Simulated gyroscope, Z-axis (rad/sec)|
|JSN2_VN|simulation’s velocity, North-axis (m/s)|
|JSN2_VE|simulation’s velocity, East-axis (m/s)|
|JSN2_VD|simulation’s velocity, Down-axis (m/s)|
|JSN2_AX|simulation’s acceleration, X-axis (m/s^2)|
|JSN2_AY|simulation’s acceleration, Y-axis (m/s^2)|
|JSN2_AZ|simulation’s acceleration, Z-axis (m/s^2)|
|JSN2_AN|simulation’s acceleration, North (m/s^2)|
|JSN2_AE|simulation’s acceleration, East (m/s^2)|
|JSN2_AD|simulation’s acceleration, Down (m/s^2)|
|LAND_stage|progress through landing sequence|
|LAND_f1|Landing flags|
|LAND_f2|Slope-specific landing flags|
|LAND_slope|Slope to landing point|
|LAND_slopeInit|Initial slope to landing point|
|LAND_altO|Rangefinder correction|
|LGR_LandingGear|Current landing gear state|
|LGR_WeightOnWheels|True if there is weight on wheels|
|MAG_MagX|magnetic field strength in body frame|
|MAG_MagY|magnetic field strength in body frame|
|MAG_MagZ|magnetic field strength in body frame|
|MAG_OfsX|magnetic field offset in body frame|
|MAG_OfsY|magnetic field offset in body frame|
|MAG_OfsZ|magnetic field offset in body frame|
|MAG_MOfsX|motor interference magnetic field offset in body frame|
|MAG_MOfsY|motor interference magnetic field offset in body frame|
|MAG_MOfsZ|motor interference magnetic field offset in body frame|
|MAG_Health|true if the compass is considered healthy|
|MAG_S|time measurement was taken|
|MAG2_MagX|magnetic field strength in body frame|
|MAG2_MagY|magnetic field strength in body frame|
|MAG2_MagZ|magnetic field strength in body frame|
|MAG2_OfsX|magnetic field offset in body frame|
|MAG2_OfsY|magnetic field offset in body frame|
|MAG2_OfsZ|magnetic field offset in body frame|
|MAG2_MOfsX|motor interference magnetic field offset in body frame|
|MAG2_MOfsY|motor interference magnetic field offset in body frame|
|MAG2_MOfsZ|motor interference magnetic field offset in body frame|
|MAG2_Health|true if the compass is considered healthy|
|MAG2_S|time measurement was taken|
|MAG3_MagX|magnetic field strength in body frame|
|MAG3_MagY|magnetic field strength in body frame|
|MAG3_MagZ|magnetic field strength in body frame|
|MAG3_OfsX|magnetic field offset in body frame|
|MAG3_OfsY|magnetic field offset in body frame|
|MAG3_OfsZ|magnetic field offset in body frame|
|MAG3_MOfsX|motor interference magnetic field offset in body frame|
|MAG3_MOfsY|motor interference magnetic field offset in body frame|
|MAG3_MOfsZ|motor interference magnetic field offset in body frame|
|MAG3_Health|true if the compass is considered healthy|
|MAG3_S|time measurement was taken|
|MAV_chan|mavlink channel number|
|MAV_txp|transmitted packet count|
|MAV_rxp|received packet count|
|MAV_rxdp|perceived number of packets we never received|
|MAV_flags|compact representation of some stage of the channel|
|MAV_ss|stream slowdown is the number of ms being added to each message to fit within bandwidth|
|MAV_tf|times buffer was full when a message was going to be sent|
|MAVC_TS|target system for command|
|MAVC_TC|target component for command|
|MAVC_Fr|command frame|
|MAVC_Cmd|mavlink command enum value|
|MAVC_Cur|current flag from mavlink packet|
|MAVC_AC|autocontinue flag from mavlink packet|
|MAVC_P1|first parameter from mavlink packet|
|MAVC_P2|second parameter from mavlink packet|
|MAVC_P3|third parameter from mavlink packet|
|MAVC_P4|fourth parameter from mavlink packet|
|MAVC_X|X coordinate from mavlink packet|
|MAVC_Y|Y coordinate from mavlink packet|
|MAVC_Z|Z coordinate from mavlink packet|
|MAVC_Res|command result being returned from autopilot|
|MAVC_WL|true if this command arrived via a COMMAND_LONG rather than COMMAND_INT|
|MMO_Nx|new measurement X axis|
|MMO_Ny|new measurement Y axis|
|MMO_Nz|new measurement Z axis|
|MMO_Ox|new offset X axis|
|MMO_Oy|new offset Y axis|
|MMO_Oz|new offset Z axis|
|MODE_Mode|vehicle-specific mode number|
|MODE_ModeNum|alias for Mode|
|MODE_Rsn|reason for entering this mode; enumeration value|
|MON_LDelay|Time main loop has been stuck for|
|MON_Task|Current scheduler task number|
|MON_IErr|Internal error mask; which internal errors have been detected|
|MON_IErrCnt|Internal error count; how many internal errors have been detected|
|MON_IErrLn|Line on which internal error ocurred|
|MON_MavMsg|Id of the last mavlink message processed|
|MON_MavCmd|Id of the last mavlink command processed|
|MON_SemLine|Line number of semaphore most recently taken|
|MON_SPICnt|Number of SPI transactions processed|
|MON_I2CCnt|Number of i2c transactions processed|
|MSG_Message|message text|
|MULT_Id|character referenced by FMTU|
|MULT_Mult|numeric multiplier|
|NKF0_ID|Beacon sensor ID|
|NKF0_rng|Beacon range|
|NKF0_innov|Beacon range innovation|
|NKF0_SIV|sqrt of beacon range innovation variance|
|NKF0_TR|Beacon range innovation consistency test ratio|
|NKF0_BPN|Beacon north position|
|NKF0_BPE|Beacon east position|
|NKF0_BPD|Beacon down position|
|NKF0_OFH|High estimate of vertical position offset of beacons rel to EKF origin|
|NKF0_OFL|Low estimate of vertical position offset of beacons rel to EKF origin|
|NKF0_OFN|always zero|
|NKF0_OFE|always zero|
|NKF0_OFD|always zero|
|NKF1_C|EKF2 core this data is for|
|NKF1_Roll|Estimated roll|
|NKF1_Pitch|Estimated pitch|
|NKF1_Yaw|Estimated yaw|
|NKF1_VN|Estimated velocity (North component)|
|NKF1_VE|Estimated velocity (East component)|
|NKF1_VD|Estimated velocity (Down component)|
|NKF1_dPD|Filtered derivative of vertical position (down)|
|NKF1_PN|Estimated distance from origin (North component)|
|NKF1_PE|Estimated distance from origin (East component)|
|NKF1_PD|Estimated distance from origin (Down component)|
|NKF1_GX|Estimated gyro bias, X axis|
|NKF1_GY|Estimated gyro bias, Y axis|
|NKF1_GZ|Estimated gyro bias, Z axis|
|NKF1_OH|Height of origin above WGS-84|
|NKF2_C|EKF2 core this data is for|
|NKF2_AZbias|Estimated accelerometer Z bias|
|NKF2_GSX|Gyro Scale Factor (X-axis)|
|NKF2_GSY|Gyro Scale Factor (Y-axis)|
|NKF2_GSZ|Gyro Scale Factor (Z-axis)|
|NKF2_VWN|Estimated wind velocity (North component)|
|NKF2_VWE|Estimated wind velocity (East component)|
|NKF2_MN|Magnetic field strength (North component)|
|NKF2_ME|Magnetic field strength (East component)|
|NKF2_MD|Magnetic field strength (Down component)|
|NKF2_MX|Magnetic field strength (body X-axis)|
|NKF2_MY|Magnetic field strength (body Y-axis)|
|NKF2_MZ|Magnetic field strength (body Z-axis)|
|NKF2_MI|Magnetometer used for data|
|NKF3_C|EKF2 core this data is for|
|NKF3_IVN|Innovation in velocity (North component)|
|NKF3_IVE|Innovation in velocity (East component)|
|NKF3_IVD|Innovation in velocity (Down component)|
|NKF3_IPN|Innovation in position (North component)|
|NKF3_IPE|Innovation in position (East component)|
|NKF3_IPD|Innovation in position (Down component)|
|NKF3_IMX|Innovation in magnetic field strength (X-axis component)|
|NKF3_IMY|Innovation in magnetic field strength (Y-axis component)|
|NKF3_IMZ|Innovation in magnetic field strength (Z-axis component)|
|NKF3_IYAW|Innovation in vehicle yaw|
|NKF3_IVT|Innovation in true-airspeed|
|NKF3_RErr|Accumulated relative error of this core with respect to active primary core|
|NKF3_ErSc|A consolidated error score where higher numbers are less healthy|
|NKF4_C|EKF2 core this data is for|
|NKF4_SV|Square root of the velocity variance|
|NKF4_SP|Square root of the position variance|
|NKF4_SH|Square root of the height variance|
|NKF4_SM|Magnetic field variance|
|NKF4_SVT|Square root of the total airspeed variance|
|NKF4_errRP|Filtered error in roll/pitch estimate|
|NKF4_OFN|Most recent position recent magnitude (North component)|
|NKF4_OFE|Most recent position recent magnitude (East component)|
|NKF4_FS|Filter fault status|
|NKF4_TS|Filter timeout status|
|NKF4_SS|Filter solution status|
|NKF4_GPS|Filter GPS status|
|NKF4_PI|Primary core index|
|NKF5_NI|Normalised flow variance|
|NKF5_FIX|Optical flow LOS rate vector innovations from the main nav filter (X-axis)|
|NKF5_FIY|Optical flow LOS rate vector innovations from the main nav filter (Y-axis)|
|NKF5_AFI|Optical flow LOS rate innovation from terrain offset estimator|
|NKF5_HAGL|Height above ground level|
|NKF5_offset|Estimated vertical position of the terrain relative to the nav filter zero datum|
|NKF5_RI|Range finder innovations|
|NKF5_rng|Measured range|
|NKF5_Herr|Filter ground offset state error|
|NKF5_eAng|Magnitude of angular error|
|NKF5_eVel|Magnitude of velocity error|
|NKF5_ePos|Magnitude of position error|
|NKQ_C|EKF2 core this data is for|
|NKQ_Q1|Quaternion a term|
|NKQ_Q2|Quaternion b term|
|NKQ_Q3|Quaternion c term|
|NKQ_Q4|Quaternion d term|
|NKT_C|EKF core this message instance applies to|
|NKT_Cnt|count of samples used to create this message|
|NKT_IMUMin|smallest IMU sample interval|
|NKT_IMUMax|largest IMU sample interval|
|NKT_EKFMin|low-passed achieved average time step rate for the EKF (minimum)|
|NKT_EKFMax|low-passed achieved average time step rate for the EKF (maximum)|
|NKT_AngMin|accumulated measurement time interval for the delta angle (minimum)|
|NKT_AngMax|accumulated measurement time interval for the delta angle (maximum)|
|NKT_VMin|accumulated measurement time interval for the delta velocity (minimum)|
|NKT_VMax|accumulated measurement time interval for the delta velocity (maximum)|
|NKY0_C|EKF2 core this data is for|
|NKY0_YC|GSF yaw estimate (rad)|
|NKY0_YCS|GSF yaw estimate 1-Sigma uncertainty (rad)|
|NKY0_Y0|Yaw estimate from individual EKF filter 0 (rad)|
|NKY0_Y1|Yaw estimate from individual EKF filter 1 (rad)|
|NKY0_Y2|Yaw estimate from individual EKF filter 2 (rad)|
|NKY0_Y3|Yaw estimate from individual EKF filter 3 (rad)|
|NKY0_Y4|Yaw estimate from individual EKF filter 4 (rad)|
|NKY0_W0|Weighting applied to yaw estimate from individual EKF filter 0|
|NKY0_W1|Weighting applied to yaw estimate from individual EKF filter 1|
|NKY0_W2|Weighting applied to yaw estimate from individual EKF filter 2|
|NKY0_W3|Weighting applied to yaw estimate from individual EKF filter 3|
|NKY0_W4|Weighting applied to yaw estimate from individual EKF filter 4|
|NKY1_C|EKF2 core this data is for|
|NKY1_IVN0|North velocity innovation from individual EKF filter 0 (m/s)|
|NKY1_IVN1|North velocity innovation from individual EKF filter 1 (m/s)|
|NKY1_IVN2|North velocity innovation from individual EKF filter 2 (m/s)|
|NKY1_IVN3|North velocity innovation from individual EKF filter 3 (m/s)|
|NKY1_IVN4|North velocity innovation from individual EKF filter 4 (m/s)|
|NKY1_IVE0|East velocity innovation from individual EKF filter 0 (m/s)|
|NKY1_IVE1|East velocity innovation from individual EKF filter 1 (m/s)|
|NKY1_IVE2|East velocity innovation from individual EKF filter 2 (m/s)|
|NKY1_IVE3|East velocity innovation from individual EKF filter 3 (m/s)|
|NKY1_IVE4|East velocity innovation from individual EKF filter 4 (m/s)|
|NTUN_Dist|distance to the current navigation waypoint|
|NTUN_TBrg|bearing to the current navigation waypoint|
|NTUN_NavBrg|the vehicle’s desired heading|
|NTUN_AltErr|difference between current vehicle height and target height|
|NTUN_XT|the vehicle’s current distance from the current travel segment|
|NTUN_XTi|integration of the vehicle’s crosstrack error|
|NTUN_AspdE|difference between vehicle’s airspeed and desired airspeed|
|NTUN_TLat|target latitude|
|NTUN_TLng|target longitude|
|NTUN_TAlt|target altitude|
|NTUN_TAspd|target airspeed|
|OABR_Type|Type of BendyRuler currently active|
|OABR_Act|True if Bendy Ruler avoidance is being used|
|OABR_DYaw|Best yaw chosen to avoid obstacle|
|OABR_Yaw|Current vehicle yaw|
|OABR_DP|Desired pitch chosen to avoid obstacle|
|OABR_RChg|True if BendyRuler resisted changing bearing and continued in last calculated bearing|
|OABR_Mar|Margin from path to obstacle on best yaw chosen|
|OABR_DLt|Destination latitude|
|OABR_DLg|Destination longitude|
|OABR_DAlt|Desired alt|
|OABR_OLt|Intermediate location chosen for avoidance|
|OABR_OLg|Intermediate location chosen for avoidance|
|OABR_OAlt|Intermediate alt chosen for avoidance|
|OADJ_State|Dijkstra avoidance library state|
|OADJ_Err|Dijkstra library error condition|
|OADJ_CurrPoint|Destination point in calculated path to destination|
|OADJ_TotPoints|Number of points in path to destination|
|OADJ_DLat|Destination latitude|
|OADJ_DLng|Destination longitude|
|OADJ_OALat|Object Avoidance chosen destination point latitude|
|OADJ_OALng|Object Avoidance chosen destination point longitude|
|OF_Qual|Estimated sensor data quality|
|OF_flowX|Sensor flow rate, X-axis|
|OF_flowY|Sensor flow rate,Y-axis|
|OF_bodyX|derived velocity, X-axis|
|OF_bodyY|derived velocity, Y-axis|
|OFG_Arsp|target airspeed cm|
|OFG_ArspA|target airspeed accel|
|OFG_Alt|target alt|
|OFG_AltA|target alt accel|
|OFG_AltF|target alt frame|
|OFG_Hdg|target heading|
|OFG_HdgA|target heading lim|
|ORGN_Type|Position type|
|ORGN_Lat|Position latitude|
|ORGN_Lng|Position longitude|
|ORGN_Alt|Position altitude|
|PARM_Name|parameter name|
|PARM_Value|parameter vlaue|
|PIDA_Tar|desired value|
|PIDA_Act|achieved value|
|PIDA_Err|error between target and achieved|
|PIDA_P|proportional part of PID|
|PIDA_I|integral part of PID|
|PIDA_D|derivative part of PID|
|PIDA_FF|controller feed-forward portion of response|
|PIDA_Dmod|scaler applied to D gain to reduce limit cycling|
|PIDG_Tar|desired value|
|PIDG_Act|achieved value|
|PIDG_Err|error between target and achieved|
|PIDG_P|proportional part of PID|
|PIDG_I|integral part of PID|
|PIDG_D|derivative part of PID|
|PIDG_FF|controller feed-forward portion of response|
|PIDG_Dmod|scaler applied to D gain to reduce limit cycling|
|PIDP_Tar|desired value|
|PIDP_Act|achieved value|
|PIDP_Err|error between target and achieved|
|PIDP_P|proportional part of PID|
|PIDP_I|integral part of PID|
|PIDP_D|derivative part of PID|
|PIDP_FF|controller feed-forward portion of response|
|PIDP_Dmod|scaler applied to D gain to reduce limit cycling|
|PIDR_Tar|desired value|
|PIDR_Act|achieved value|
|PIDR_Err|error between target and achieved|
|PIDR_P|proportional part of PID|
|PIDR_I|integral part of PID|
|PIDR_D|derivative part of PID|
|PIDR_FF|controller feed-forward portion of response|
|PIDR_Dmod|scaler applied to D gain to reduce limit cycling|
|PIDS_Tar|desired value|
|PIDS_Act|achieved value|
|PIDS_Err|error between target and achieved|
|PIDS_P|proportional part of PID|
|PIDS_I|integral part of PID|
|PIDS_D|derivative part of PID|
|PIDS_FF|controller feed-forward portion of response|
|PIDS_Dmod|scaler applied to D gain to reduce limit cycling|
|PIDY_Tar|desired value|
|PIDY_Act|achieved value|
|PIDY_Err|error between target and achieved|
|PIDY_P|proportional part of PID|
|PIDY_I|integral part of PID|
|PIDY_D|derivative part of PID|
|PIDY_FF|controller feed-forward portion of response|
|PIDY_Dmod|scaler applied to D gain to reduce limit cycling|
|PIQA_Tar|desired value|
|PIQA_Act|achieved value|
|PIQA_Err|error between target and achieved|
|PIQA_P|proportional part of PID|
|PIQA_I|integral part of PID|
|PIQA_D|derivative part of PID|
|PIQA_FF|controller feed-forward portion of response|
|PIQA_Dmod|scaler applied to D gain to reduce limit cycling|
|PIQP_Tar|desired value|
|PIQP_Act|achieved value|
|PIQP_Err|error between target and achieved|
|PIQP_P|proportional part of PID|
|PIQP_I|integral part of PID|
|PIQP_D|derivative part of PID|
|PIQP_FF|controller feed-forward portion of response|
|PIQP_Dmod|scaler applied to D gain to reduce limit cycling|
|PIQR_Tar|desired value|
|PIQR_Act|achieved value|
|PIQR_Err|error between target and achieved|
|PIQR_P|proportional part of PID|
|PIQR_I|integral part of PID|
|PIQR_D|derivative part of PID|
|PIQR_FF|controller feed-forward portion of response|
|PIQR_Dmod|scaler applied to D gain to reduce limit cycling|
|PIQY_Tar|desired value|
|PIQY_Act|achieved value|
|PIQY_Err|error between target and achieved|
|PIQY_P|proportional part of PID|
|PIQY_I|integral part of PID|
|PIQY_D|derivative part of PID|
|PIQY_FF|controller feed-forward portion of response|
|PIQY_Dmod|scaler applied to D gain to reduce limit cycling|
|PM_NLon|Number of long loops detected|
|PM_NLoop|Number of measurement loops for this message|
|PM_MaxT|Maximum loop time|
|PM_Mem|Free memory available|
|PM_Load|System processor load|
|PM_IntE|Internal error mask; which internal errors have been detected|
|PM_IntEC|Internal error count; how many internal errors have been detected|
|PM_SPIC|Number of SPI transactions processed|
|PM_I2CC|Number of i2c transactions processed|
|PM_I2CI|Number of i2c interrupts serviced|
|PM_ExUS|number of microseconds being added to each loop to address scheduler overruns|
|POS_Lat|Canonical vehicle latitude|
|POS_Lng|Canonical vehicle longitude|
|POS_Alt|Canonical vehicle altitude|
|POS_RelHomeAlt|Canonical vehicle altitude relative to home|
|POS_RelOriginAlt|Canonical vehicle altitude relative to navigation origin|
|POWR_Vcc|Flight board voltage|
|POWR_VServo|Servo rail voltage|
|POWR_Flags|System power flags|
|POWR_AccFlags|Accumulated System power flags; all flags which have ever been set|
|POWR_Safety|Hardware Safety Switch status|
|PRTN_Set|Parameter set being tuned|
|PRTN_Parm|Parameter being tuned|
|PRTN_Value|Current parameter value|
|PRTN_CenterValue|Center value (startpoint of current modifications) of parameter being tuned|
|PRX_Health|True if proximity sensor is healthy|
|PRX_D0|Nearest object in sector surrounding 0-degrees|
|PRX_D45|Nearest object in sector surrounding 45-degrees|
|PRX_D90|Nearest object in sector surrounding 90-degrees|
|PRX_D135|Nearest object in sector surrounding 135-degrees|
|PRX_D180|Nearest object in sector surrounding 180-degrees|
|PRX_D225|Nearest object in sector surrounding 225-degrees|
|PRX_D270|Nearest object in sector surrounding 270-degrees|
|PRX_D315|Nearest object in sector surrounding 315-degrees|
|PRX_DUp|Nearest object in upwards direction|
|PRX_CAn|Angle to closest object|
|PRX_CDis|Distance to closest object|
|PSC_TPX|Target position relative to origin, X-axis|
|PSC_TPY|Target position relative to origin, Y-axis|
|PSC_PX|Position relative to origin, X-axis|
|PSC_PY|Position relative to origin, Y-axis|
|PSC_TVX|Target velocity, X-axis|
|PSC_TVY|Target velocity, Y-axis|
|PSC_VX|Velocity, X-axis|
|PSC_VY|Velocity, Y-axis|
|PSC_TAX|Target acceleration, X-axis|
|PSC_TAY|Target acceleration, Y-axis|
|PSC_AX|Acceleration, X-axis|
|PSC_AY|Acceleration, Y-axis|
|QTUN_ThI|throttle input|
|QTUN_ABst|angle boost|
|QTUN_ThO|throttle output|
|QTUN_ThH|calculated hover throttle|
|QTUN_DAlt|desired altitude|
|QTUN_Alt|achieved altitude|
|QTUN_BAlt|barometric altitude|
|QTUN_DCRt|desired climb rate|
|QTUN_CRt|climb rate|
|QTUN_TMix|transition throttle mix value|
|QTUN_Sscl|speed scalar for tailsitter control surfaces|
|QTUN_Trans|Transistion state|
|RAD_RSSI|RSSI|
|RAD_RemRSSI|RSSI reported from remote radio|
|RAD_TxBuf|number of bytes in radio ready to be sent|
|RAD_Noise|local noise floor|
|RAD_RemNoise|local noise floor reported from remote radio|
|RAD_RxErrors|damaged packet count|
|RAD_Fixed|fixed damaged packet count|
|RALY_Tot|total number of rally points onboard|
|RALY_Seq|this rally point’s sequence number|
|RALY_Lat|latitude of rally point|
|RALY_Lng|longitude of rally point|
|RALY_Alt|altitude of rally point|
|RATE_RDes|vehicle desired roll rate|
|RATE_R|achieved vehicle roll rate|
|RATE_ROut|normalized output for Roll|
|RATE_PDes|vehicle desired pitch rate|
|RATE_P|vehicle pitch rate|
|RATE_POut|normalized output for Pitch|
|RATE_YDes|vehicle desired yaw rate|
|RATE_Y|achieved vehicle yaw rate|
|RATE_YOut|normalized output for Yaw|
|RATE_ADes|desired vehicle vertical acceleration|
|RATE_A|achieved vehicle vertical acceleration|
|RATE_AOut|percentage of vertical thrust output current being used|
|RCDA_TS|data arrival timestamp|
|RCDA_Prot|Protocol currently being decoded|
|RCDA_Len|Number of valid bytes in message|
|RCDA_U0|first quartet of bytes|
|RCDA_U1|second quartet of bytes|
|RCDA_U2|third quartet of bytes|
|RCDA_U3|fourth quartet of bytes|
|RCDA_U4|fifth quartet of bytes|
|RCDA_U5|sixth quartet of bytes|
|RCDA_U6|seventh quartet of bytes|
|RCDA_U7|eight quartet of bytes|
|RCDA_U8|ninth quartet of bytes|
|RCDA_U9|tenth quartet of bytes|
|RCIN_C1|channel 1 input|
|RCIN_C2|channel 2 input|
|RCIN_C3|channel 3 input|
|RCIN_C4|channel 4 input|
|RCIN_C5|channel 5 input|
|RCIN_C6|channel 6 input|
|RCIN_C7|channel 7 input|
|RCIN_C8|channel 8 input|
|RCIN_C9|channel 9 input|
|RCIN_C10|channel 10 input|
|RCIN_C11|channel 11 input|
|RCIN_C12|channel 12 input|
|RCIN_C13|channel 13 input|
|RCIN_C14|channel 14 input|
|RCOU_C1|channel 1 output|
|RCOU_C2|channel 2 output|
|RCOU_C3|channel 3 output|
|RCOU_C4|channel 4 output|
|RCOU_C5|channel 5 output|
|RCOU_C6|channel 6 output|
|RCOU_C7|channel 7 output|
|RCOU_C8|channel 8 output|
|RCOU_C9|channel 9 output|
|RCOU_C10|channel 10 output|
|RCOU_C11|channel 11 output|
|RCOU_C12|channel 12 output|
|RCOU_C13|channel 13 output|
|RCOU_C14|channel 14 output|
|RFND_Instance|rangefinder instance number this data is from|
|RFND_Dist|Reported distance from sensor|
|RFND_Stat|Sensor state|
|RFND_Orient|Sensor orientation|
|RPM_rpm1|First sensor’s data|
|RPM_rpm2|Second sensor’s data|
|RSSI_RXRSSI|RSSI|
|SA_State|True if Simple Avoidance is active|
|SA_DVelX|Desired velocity, X-Axis (Velocity before Avoidance)|
|SA_DVelY|Desired velocity, Y-Axis (Velocity before Avoidance)|
|SA_MVelX|Modified velocity, X-Axis (Velocity after Avoidance)|
|SA_MVelY|Modified velocity, Y-Axis (Velocity after Avoidance)|
|SA_Back|True if vehicle is backing away|
|SBPH_CrcError|Number of packet CRC errors on serial connection|
|SBPH_LastInject|Timestamp of last raw data injection to GPS|
|SBPH_IARhyp|Current number of integer ambiguity hypotheses|
|SBRH_msg_flag|Swift message type|
|SBRH_1|Sender ID|
|SBRH_2|index; always 1|
|SBRH_3|pages; number of pages received|
|SBRH_4|msg length; number of bytes received|
|SBRH_5|unused; always zero|
|SBRH_6|data received from device|
|SIM_Roll|Simulated roll|
|SIM_Pitch|Simulated pitch|
|SIM_Yaw|Simulated yaw|
|SIM_Alt|Simulated altitude|
|SIM_Lat|Simulated latitude|
|SIM_Lng|Simulated longitude|
|SIM_Q1|Attitude quaternion component 1|
|SIM_Q2|Attitude quaternion component 2|
|SIM_Q3|Attitude quaternion component 3|
|SIM_Q4|Attitude quaternion component 4|
|SITL_VN|Velocity - North component|
|SITL_VE|Velocity - East component|
|SITL_VD|Velocity - Down component|
|SITL_AN|Acceleration - North component|
|SITL_AE|Acceleration - East component|
|SITL_AD|Acceleration - Down component|
|SITL_PN|Position - North component|
|SITL_PE|Position - East component|
|SITL_PD|Position - Down component|
|SMOO_AEx|Angular Velocity (around x-axis)|
|SMOO_AEy|Angular Velocity (around y-axis)|
|SMOO_AEz|Angular Velocity (around z-axis)|
|SMOO_DPx|Velocity (along x-axis)|
|SMOO_DPy|Velocity (along y-axis)|
|SMOO_DPz|Velocity (along z-axis)|
|SMOO_R|Roll|
|SMOO_P|Pitch|
|SMOO_Y|Yaw|
|SMOO_R2|DCM Roll|
|SMOO_P2|DCM Pitch|
|SMOO_Y2|DCM Yaw|
|SOAR_nettorate|Estimate of vertical speed of surrounding airmass|
|SOAR_x0|Thermal strength estimate|
|SOAR_x1|Thermal radius estimate|
|SOAR_x2|Thermal position estimate north from home|
|SOAR_x3|Thermal position estimate east from home|
|SOAR_north|Aircraft position north from home|
|SOAR_east|Aircraft position east from home|
|SOAR_alt|Aircraft altitude|
|SOAR_dx_w|Wind speed north|
|SOAR_dy_w|Wind speed east|
|SOAR_th|Estimate of achievable climbrate in thermal|
|SRTL_Active|true if SmartRTL could be used right now|
|SRTL_NumPts|number of points currently in use|
|SRTL_MaxPts|maximum number of points that could be used|
|SRTL_Action|most recent internal action taken by SRTL library|
|SRTL_N|point associated with most recent action (North component)|
|SRTL_E|point associated with most recent action (East component)|
|SRTL_D|point associated with most recent action (Down component)|
|STAT_isFlying|True if aircraft is probably flying|
|STAT_isFlyProb|Probabilty that the aircraft is flying|
|STAT_Armed|Arm status of the aircraft|
|STAT_Safety|State of the safety switch|
|STAT_Crash|True if crash is detected|
|STAT_Still|True when vehicle is not moving in any axis|
|STAT_Stage|Current stage of the flight|
|STAT_Hit|True if impact is detected|
|TEC2_pmax|maximum allowed pitch from parameter|
|TEC2_pmin|minimum allowed pitch from parameter|
|TEC2_KErr|difference between estimated kinetic energy and desired kinetic energy|
|TEC2_PErr|difference between estimated potential energy and desired potential energy|
|TEC2_EDelta|current error in speed/balance weighting|
|TEC2_LF|aerodynamic load factor|
|TECS_h|height estimate (UP) currently in use by TECS|
|TECS_dh|current climb rate (“delta-height”)|
|TECS_hdem|height TECS is currently trying to achieve|
|TECS_dhdem|climb rate TECS is currently trying to achieve|
|TECS_spdem|True AirSpeed TECS is currently trying to achieve|
|TECS_sp|current estimated True AirSpeed|
|TECS_dsp|x-axis acceleration estimate (“delta-speed”)|
|TECS_ith|throttle integrator value|
|TECS_iph|Specific Energy Balance integrator value|
|TECS_th|throttle output|
|TECS_ph|pitch output|
|TECS_dspdem|demanded acceleration output (“delta-speed demand”)|
|TECS_w|current TECS prioritization of height vs speed (0==100% height,2==100% speed, 1==50%height+50%speed|
|TECS_f|flags|
|TERR_Status|Terrain database status|
|TERR_Lat|Current vehicle latitude|
|TERR_Lng|Current vehicle longitude|
|TERR_Spacing|terrain Tile spacing|
|TERR_TerrH|current Terrain height|
|TERR_CHeight|Vehicle height above terrain|
|TERR_Pending|Number of tile requests outstanding|
|TERR_Loaded|Number of tiles in memory|
|TRIG_GPSTime|milliseconds since start of GPS week|
|TRIG_GPSWeek|weeks since 5 Jan 1980|
|TRIG_Lat|current latitude|
|TRIG_Lng|current longitude|
|TRIG_Alt|current altitude|
|TRIG_RelAlt|current altitude relative to home|
|TRIG_GPSAlt|altitude as reported by GPS|
|TRIG_Roll|current vehicle roll|
|TRIG_Pitch|current vehicle pitch|
|TRIG_Yaw|current vehicle yaw|
|TSYN_SysID|system ID this data is for|
|TSYN_RTT|round trip time for this system|
|UBX1_Instance|GPS instance number|
|UBX1_noisePerMS|noise level as measured by GPS|
|UBX1_jamInd|jamming indicator; higher is more likely jammed|
|UBX1_aPower|antenna power indicator; 2 is don’t know|
|UBX1_agcCnt|automatic gain control monitor|
|UBX1_config|bitmask for messages which haven’t been seen|
|UBX2_Instance|GPS instance number|
|UBX2_ofsI|imbalance of I part of complex signal|
|UBX2_magI|magnitude of I part of complex signal|
|UBX2_ofsQ|imbalance of Q part of complex signal|
|UBX2_magQ|magnitude of Q part of complex signal|
|UNIT_Id|character referenced by FMTU|
|UNIT_Label|Unit - SI where available|
|VAR_aspd_raw|always zero|
|VAR_aspd_filt|filtered and constrained airspeed|
|VAR_alt|AHRS altitude|
|VAR_roll|AHRS roll|
|VAR_raw|estimated air vertical speed|
|VAR_filt|low-pass filtered air vertical speed|
|VAR_cl|raw climb rate|
|VAR_fc|filtered climb rate|
|VAR_exs|expected sink rate relative to air in thermalling turn|
|VAR_dsp|average acceleration along X axis|
|VAR_dspb|detected bias in average acceleration along X axis|
|VIBE_IMU|Vibration instance number|
|VIBE_VibeX|Primary accelerometer filtered vibration, x-axis|
|VIBE_VibeY|Primary accelerometer filtered vibration, y-axis|
|VIBE_VibeZ|Primary accelerometer filtered vibration, z-axis|
|VIBE_Clip|Number of clipping events on 1st accelerometer|
|VISO_dt|Time period this data covers|
|VISO_AngDX|Angular change for body-frame roll axis|
|VISO_AngDY|Angular change for body-frame pitch axis|
|VISO_AngDZ|Angular change for body-frame z axis|
|VISO_PosDX|Position change for body-frame X axis (Forward-Back)|
|VISO_PosDY|Position change for body-frame Y axis (Right-Left)|
|VISO_PosDZ|Position change for body-frame Z axis (Down-Up)|
|VISO_conf|Confidence|
|VISP_CTimeMS|Corrected system time|
|VISP_PX|Position X-axis (North-South)|
|VISP_PY|Position Y-axis (East-West)|
|VISP_PZ|Position Z-axis (Down-Up)|
|VISP_Roll|Roll lean angle|
|VISP_Pitch|Pitch lean angle|
|VISP_Yaw|Yaw angle|
|VISP_PErr|Position estimate error|
|VISP_AErr|Attitude estimate error|
|VISP_Rst|Position reset counter|
|VISP_Ign|Ignored|
|VISV_CTimeMS|Corrected system time|
|VISV_VX|Velocity X-axis (North-South)|
|VISV_VY|Velocity Y-axis (East-West)|
|VISV_VZ|Velocity Z-axis (Down-Up)|
|VISV_VErr|Velocity estimate error|
|VISV_Rst|Velocity reset counter|
|VISV_Ign|Ignored|
|WENC_Dist0|First wheel distance travelled|
|WENC_Qual0|Quality measurement of Dist0|
|WENC_Dist1|Second wheel distance travelled|
|WENC_Qual1|Quality measurement of Dist1|
|WINC_Heal|Healthy|
|WINC_ThEnd|Reached end of thread|
|WINC_Mov|Motor is moving|
|WINC_Clut|Clutch is engaged (motor can move freely)|
|WINC_Mode|0 is Relaxed, 1 is Position Control, 2 is Rate Control|
|WINC_DLen|Desired Length|
|WINC_Len|Estimated Length|
|WINC_DRate|Desired Rate|
|WINC_Tens|Tension on line|
|WINC_Vcc|Voltage to Motor|
|WINC_Temp|Motor temperature|
|XKF0_ID|Beacon sensor ID|
|XKF0_rng|Beacon range|
|XKF0_innov|Beacon range innovation|
|XKF0_SIV|sqrt of beacon range innovation variance|
|XKF0_TR|Beacon range innovation consistency test ratio|
|XKF0_BPN|Beacon north position|
|XKF0_BPE|Beacon east position|
|XKF0_BPD|Beacon down position|
|XKF0_OFH|High estimate of vertical position offset of beacons rel to EKF origin|
|XKF0_OFL|Low estimate of vertical position offset of beacons rel to EKF origin|
|XKF0_OFN|North position of receiver rel to EKF origin|
|XKF0_OFE|East position of receiver rel to EKF origin|
|XKF0_OFD|Down position of receiver rel to EKF origin|
|XKF1_C|EKF3 core this data is for|
|XKF1_Roll|Estimated roll|
|XKF1_Pitch|Estimated pitch|
|XKF1_Yaw|Estimated yaw|
|XKF1_VN|Estimated velocity (North component)|
|XKF1_VE|Estimated velocity (East component)|
|XKF1_VD|Estimated velocity (Down component)|
|XKF1_dPD|Filtered derivative of vertical position (down)|
|XKF1_PN|Estimated distance from origin (North component)|
|XKF1_PE|Estimated distance from origin (East component)|
|XKF1_PD|Estimated distance from origin (Down component)|
|XKF1_GX|Estimated gyro bias, X axis|
|XKF1_GY|Estimated gyro bias, Y axis|
|XKF1_GZ|Estimated gyro bias, Z axis|
|XKF1_OH|Height of origin above WGS-84|
|XKF2_C|EKF3 core this data is for|
|XKF2_AX|Estimated accelerometer X bias|
|XKF2_AY|Estimated accelerometer Y bias|
|XKF2_AZ|Estimated accelerometer Z bias|
|XKF2_VWN|Estimated wind velocity (North component)|
|XKF2_VWE|Estimated wind velocity (East component)|
|XKF2_MN|Magnetic field strength (North component)|
|XKF2_ME|Magnetic field strength (East component)|
|XKF2_MD|Magnetic field strength (Down component)|
|XKF2_MX|Magnetic field strength (body X-axis)|
|XKF2_MY|Magnetic field strength (body Y-axis)|
|XKF2_MZ|Magnetic field strength (body Z-axis)|
|XKF3_C|EKF3 core this data is for|
|XKF3_IVN|Innovation in velocity (North component)|
|XKF3_IVE|Innovation in velocity (East component)|
|XKF3_IVD|Innovation in velocity (Down component)|
|XKF3_IPN|Innovation in position (North component)|
|XKF3_IPE|Innovation in position (East component)|
|XKF3_IPD|Innovation in position (Down component)|
|XKF3_IMX|Innovation in magnetic field strength (X-axis component)|
|XKF3_IMY|Innovation in magnetic field strength (Y-axis component)|
|XKF3_IMZ|Innovation in magnetic field strength (Z-axis component)|
|XKF3_IYAW|Innovation in vehicle yaw|
|XKF3_IVT|Innovation in true-airspeed|
|XKF3_RErr|Accumulated relative error of this core with respect to active primary core|
|XKF3_ErSc|A consolidated error score where higher numbers are less healthy|
|XKF4_C|EKF3 core this data is for|
|XKF4_SV|Square root of the velocity variance|
|XKF4_SP|Square root of the position variance|
|XKF4_SH|Square root of the height variance|
|XKF4_SM|Magnetic field variance|
|XKF4_SVT|Square root of the total airspeed variance|
|XKF4_errRP|Filtered error in roll/pitch estimate|
|XKF4_OFN|Most recent position recent magnitude (North component)|
|XKF4_OFE|Most recent position recent magnitude (East component)|
|XKF4_FS|Filter fault status|
|XKF4_TS|Filter timeout status|
|XKF4_SS|Filter solution status|
|XKF4_GPS|Filter GPS status|
|XKF4_PI|Primary core index|
|XKF5_NI|Normalised flow variance|
|XKF5_FIX|Optical flow LOS rate vector innovations from the main nav filter (X-axis)|
|XKF5_FIY|Optical flow LOS rate vector innovations from the main nav filter (Y-axis)|
|XKF5_AFI|Optical flow LOS rate innovation from terrain offset estimator|
|XKF5_HAGL|Height above ground level|
|XKF5_offset|Estimated vertical position of the terrain relative to the nav filter zero datum|
|XKF5_RI|Range finder innovations|
|XKF5_rng|Measured range|
|XKF5_Herr|Filter ground offset state error|
|XKF5_eAng|Magnitude of angular error|
|XKF5_eVel|Magnitude of velocity error|
|XKF5_ePos|Magnitude of position error|
|XKFD_IX|Innovation in velocity (X-axis)|
|XKFD_IY|Innovation in velocity (Y-axis)|
|XKFD_IZ|Innovation in velocity (Z-axis)|
|XKFD_IVX|Variance in velocity (X-axis)|
|XKFD_IVY|Variance in velocity (Y-axis)|
|XKFD_IVZ|Variance in velocity (Z-axis)|
|XKFM_OGNM|True of on ground and not moving|
|XKFM_GLR|Gyroscope length ratio|
|XKFM_ALR|Accelerometer length ratio|
|XKFM_GDR|Gyroscope rate of change ratio|
|XKFM_ADR|Accelerometer rate of change ratio|
|XKFS_C|EKF3 core this data is for|
|XKFS_MI|compass selection index|
|XKFS_BI|barometer selection index|
|XKFS_GI|GPS selection index|
|XKFS_AI|airspeed selection index|
|XKQ_C|EKF3 core this data is for|
|XKQ_Q1|Quaternion a term|
|XKQ_Q2|Quaternion b term|
|XKQ_Q3|Quaternion c term|
|XKQ_Q4|Quaternion d term|
|XKT_C|EKF core this message instance applies to|
|XKT_Cnt|count of samples used to create this message|
|XKT_IMUMin|smallest IMU sample interval|
|XKT_IMUMax|largest IMU sample interval|
|XKT_EKFMin|low-passed achieved average time step rate for the EKF (minimum)|
|XKT_EKFMax|low-passed achieved average time step rate for the EKF (maximum)|
|XKT_AngMin|accumulated measurement time interval for the delta angle (minimum)|
|XKT_AngMax|accumulated measurement time interval for the delta angle (maximum)|
|XKT_VMin|accumulated measurement time interval for the delta velocity (minimum)|
|XKT_VMax|accumulated measurement time interval for the delta velocity (maximum)|
|XKTV_C|EKF3 core this data is for|
|XKTV_TVS|Tilt Error Variance from symboolic equations (rad^2)|
|XKTV_TVD|Tilt Error Variance from difference method (rad^2)|
|XKV1_V00|Variance for state 0|
|XKV1_V01|Variance for state 1|
|XKV1_V02|Variance for state 2|
|XKV1_V03|Variance for state 3|
|XKV1_V04|Variance for state 4|
|XKV1_V05|Variance for state 5|
|XKV1_V06|Variance for state 6|
|XKV1_V07|Variance for state 7|
|XKV1_V08|Variance for state 8|
|XKV1_V09|Variance for state 9|
|XKV1_V10|Variance for state 10|
|XKV1_V11|Variance for state 11|
|XKV2_V12|Variance for state 12|
|XKV2_V13|Variance for state 13|
|XKV2_V14|Variance for state 14|
|XKV2_V15|Variance for state 15|
|XKV2_V16|Variance for state 16|
|XKV2_V17|Variance for state 17|
|XKV2_V18|Variance for state 18|
|XKV2_V19|Variance for state 19|
|XKV2_V20|Variance for state 20|
|XKV2_V21|Variance for state 21|
|XKV2_V22|Variance for state 22|
|XKV2_V23|Variance for state 23|
|XKY0_C|EKF3 core this data is for|
|XKY0_YC|GSF yaw estimate (rad)|
|XKY0_YCS|GSF yaw estimate 1-Sigma uncertainty (rad)|
|XKY0_Y0|Yaw estimate from individual EKF filter 0 (rad)|
|XKY0_Y1|Yaw estimate from individual EKF filter 1 (rad)|
|XKY0_Y2|Yaw estimate from individual EKF filter 2 (rad)|
|XKY0_Y3|Yaw estimate from individual EKF filter 3 (rad)|
|XKY0_Y4|Yaw estimate from individual EKF filter 4 (rad)|
|XKY0_W0|Weighting applied to yaw estimate from individual EKF filter 0|
|XKY0_W1|Weighting applied to yaw estimate from individual EKF filter 1|
|XKY0_W2|Weighting applied to yaw estimate from individual EKF filter 2|
|XKY0_W3|Weighting applied to yaw estimate from individual EKF filter 3|
|XKY0_W4|Weighting applied to yaw estimate from individual EKF filter 4|
|XKY1_C|EKF3 core this data is for|
|XKY1_IVN0|North velocity innovation from individual EKF filter 0 (m/s)|
|XKY1_IVN1|North velocity innovation from individual EKF filter 1 (m/s)|
|XKY1_IVN2|North velocity innovation from individual EKF filter 2 (m/s)|
|XKY1_IVN3|North velocity innovation from individual EKF filter 3 (m/s)|
|XKY1_IVN4|North velocity innovation from individual EKF filter 4 (m/s)|
|XKY1_IVE0|East velocity innovation from individual EKF filter 0 (m/s)|
|XKY1_IVE1|East velocity innovation from individual EKF filter 1 (m/s)|
|XKY1_IVE2|East velocity innovation from individual EKF filter 2 (m/s)|
|XKY1_IVE3|East velocity innovation from individual EKF filter 3 (m/s)|
|XKY1_IVE4|East velocity innovation from individual EKF filter 4 (m/s)|
