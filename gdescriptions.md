| feature name | description |
| ------ | ------ |
|ACC1|IMU accelerometer data|
|ACC2|IMU accelerometer data|
|ACC3|IMU accelerometer data|
|ADSB|Automatic Dependant Serveillance - Broadcast detected vehicle information|
|AETR|Normalised pre-mixer control surface outputs|
|AHR2|Backup AHRS data|
|AOA|Angle of attack and Side Slip Angle values|
|ARM|Arming status changes|
|AROT|Helicopter AutoRotation information|
|ARSP|Airspeed sensor data|
|ASM1|AirSim simulation data|
|ASM2|More AirSim simulation data|
|ASP2|Airspeed sensor data|
|ATDE|AutoTune data packet|
|ATRP|Pitch/Roll AutoTune messages for Plane|
|ATT|Canonical vehicle attitude|
|ATUN|Copter/QuadPlane AutoTune|
|BAR2|Gathered Barometer data|
|BAR3|Gathered Barometer data|
|BARO|Gathered Barometer data|
|BAT|Gathered battery data|
|BCL|Battery cell voltage information|
|BCN|Beacon informtaion|
|CAM|Camera shutter information|
|CESC|CAN ESC data|
|CMD|Executed mission command information|
|CMDI|Generic CommandInt message logger(CMDI)|
|COFS|Current compass learn offsets|
|CSRV|Servo feedback data|
|CTRL|Attitude Control oscillation monitor diagnostics|
|CTUN|Control Tuning information|
|DMS|DataFlash-Over-MAVLink statistics|
|DSF|Onboard logging statistics|
|DSTL|Deepstall Landing data|
|ECYL|EFI per-cylinder information|
|EFI|Electronic Fuel Injection system data|
|EFI2|Electronic Fuel Injection system data - redux|
|ERR|Specifically coded error messages|
|ESC|Feedback received from ESCs|
|EV|Specifically coded event messages|
|FMT|Message defining the format of messages in this file|
|FMTU|Message defining units and multipliers used for fields of other messages|
|FOLL|Follow library diagnostic data|
|FTN|Filter Tuning Messages|
|FTN1|FFT Filter Tuning|
|FTN2|FFT Noise Frequency Peak|
|GPA|GPS accuracy information|
|GPA2|GPS accuracy information|
|GPAB|Blended GPS accuracy information|
|GPS|Information received from GNSS systems attached to the autopilot|
|GPS2|Information received from GNSS systems attached to the autopilot|
|GPSB|Information blended from GNSS systems attached to the autopilot|
|GRAW|Raw uBlox data|
|GRXH|Raw uBlox data - header|
|GRXS|Raw uBlox data - space-vehicle data|
|GYR1|IMU gyroscope data|
|GYR2|IMU gyroscope data|
|GYR3|IMU gyroscope data|
|HEAT|IMU Heater data|
|ICMB|ICM20789 diagnostics|
|IMT|Inertial Measurement Unit timing data|
|IMT2|Inertial Measurement Unit timing data|
|IMT3|Inertial Measurement Unit timing data|
|IMU|Inertial Measurement Unit data|
|IMU2|Inertial Measurement Unit data|
|IMU3|Inertial Measurement Unit data|
|IOMC|IOMCU diagnostic information|
|JSN1|Log data received from JSON simulator|
|JSN2|Log data received from JSON simulator|
|LAND|Slope Landing data|
|LGR|Landing gear information|
|MAG|Information received from compasses|
|MAG2|Information received from compasses|
|MAG3|Information received from compasses|
|MAV|GCS MAVLink link statistics|
|MAVC|MAVLink command we have just executed|
|MMO|MMC3416 compass data|
|MODE|vehicle control mode information|
|MON|Main loop stuck data|
|MSG|Textual messages|
|MULT|Message mapping from single character to numeric multiplier|
|NKF0|EKF2 beacon sensor diagnostics|
|NKF1|EKF2 estimator outputs|
|NKF2|EKF2 estimator secondary outputs|
|NKF3|EKF2 innovations|
|NKF4|EKF2 variances|
|NKF5|EKF2 Sensor innovations (primary core) and general dumping ground|
|NKQ|EKF2 quaternion defining the rotation from NED to XYZ (autopilot) axes|
|NKT|EKF timing information|
|NKY0|EKF2 Yaw Estimator States|
|NKY1|EKF2 Yaw Estimator Innovations|
|NTUN|Navigation Tuning information - e.g. vehicle destination|
|OABR|Object avoidance (Bendy Ruler) diagnostics|
|OADJ|Object avoidance (Dijkstra) diagnostics|
|OF|Optical flow sensor data|
|OFG|OFfboard-Guided - an advanced version of GUIDED for companion computers that includes rate/s.|
|ORGN|Vehicle navigation origin or other notable position|
|PARM|parameter value|
|PIDA|Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Altitude/Steering|
|PIDG|Plane Proportional/Integral/Derivative gain values for Heading when using COMMAND_INT control.|
|PIDP|Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Altitude/Steering|
|PIDR|Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Altitude/Steering|
|PIDS|Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Altitude/Steering|
|PIDY|Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Altitude/Steering|
|PIQA|QuadPlane Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Z|
|PIQP|QuadPlane Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Z|
|PIQR|QuadPlane Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Z|
|PIQY|QuadPlane Proportional/Integral/Derivative gain values for Roll/Pitch/Yaw/Z|
|PM|autopilot system performance and general data dumping ground|
|POS|Canonical vehicle position|
|POWR|System power information|
|PRTN|Plane Parameter Tuning data|
|PRX|Proximity sensor data|
|PSC|Position Control data|
|QTUN|QuadPlane vertical tuning message|
|RAD|Telemetry radio statistics|
|RALY|Rally point information|
|RATE|Desired and achieved vehicle attitude rates|
|RCDA|Raw RC data|
|RCIN|RC input channels to vehicle|
|RCOU|Servo channel output values|
|RFND|Rangefinder sensor information|
|RPM|Data from RPM sensors|
|RSSI|Received Signal Strength Indicator for RC receiver|
|SA|Simple Avoidance messages|
|SBPH|Swift Health Data|
|SBRH|Swift Raw Message Data|
|SIM|SITL simulator state|
|SITL|Simulation data|
|SMOO|Smoothed sensor data fed to EKF to avoid inconsistencies|
|SOAR|Logged data from soaring feature|
|SRTL|SmartRTL statistics|
|STAT|Current status of the aircraft|
|TEC2|Additional Information about the Total Energy Control System|
|TECS|Information about the Total Energy Control System|
|TERR|Terrain database infomration|
|TRIG|Camera shutter information|
|TSYN|Time synchronisation response information|
|UBX1|uBlox-specific GPS information (part 1)|
|UBX2|uBlox-specific GPS information (part 2)|
|UNIT|Message mapping from single character to SI unit|
|VAR|Variometer data|
|VIBE|Processed (acceleration) vibration information|
|VISO|Visual Odometry|
|VISP|Vision Position|
|VISV|Vision Velocity|
|WENC|Wheel encoder measurements|
|WINC|Winch|
|XKF0|EKF3 beacon sensor diagnostics|
|XKF1|EKF3 estimator outputs|
|XKF2|EKF3 estimator secondary outputs|
|XKF3|EKF3 innovations|
|XKF4|EKF3 variances|
|XKF5|EKF3 Sensor innovations (primary core) and general dumping ground|
|XKFD|EKF3 Body Frame Odometry errors|
|XKFM|EKF3 diagnostic data for on-ground-and-not-moving check|
|XKFS|EKF3 sensor selection|
|XKQ|EKF3 quaternion defining the rotation from NED to XYZ (autopilot) axes|
|XKT|EKF timing information|
|XKTV|EKF3 Yaw Estimator States|
|XKV1|EKF3 State variances (primary core)|
|XKV2|more EKF3 State Variances (primary core)|
|XKY0|EKF3 Yaw Estimator States|
|XKY1|EKF2 Yaw Estimator Innovations|
