#!/usr/bin/env python3

import BSLog.DataflashParser as dfp
import sys
import re
import argparse

# argument parser
parser = argparse.ArgumentParser(
        description='Inferer for battery current'
        ' consumption in simulated dataflash logfiles. Only applicable for'
        ' forward flight mode (isFlying=1, Stage=3) in Beagle Systems'
        ' UAVs')
parser.add_argument('ifname', metavar='filename',
                    help='Dataflash log as CSV file')
parser.add_argument('msg', metavar='message-log',
                    help='Corresponding message log file for Dataflash log'
                         ' to identify the flight mode times')
parser.add_argument('--real', action="store_true", help='set this flag if your'
                    ' input is a real Dataflash log not a simulated one')


# model parameters
betas = {
    ('ARSP_Airspeed', 'm/s'): 2.438505,
    ('TECS_dsp', 'm/s')     : 7.874059,
    ('XKF1_Pitch', 'deg')   : 131.176547,
    ('NTUN_AltErr', 'm')    : 24.749065
}
constant = -35.376203
target = ('BAT_Curr', 'A')

# the flight mode ('MODE' or 'STAT') the model applies for
mode_kind = 'STAT'
flight_mode = 'isFlying1_Stage3'


# helper functions
def read_msglog(ifname: str):
    msg_log = list()
    with open(ifname, 'r') as sr:
        for line in sr:
            m = re.search(r'^\d', line)
            if m:
                line = line.rstrip()
                time, txt = line.split('\t')
                msg_log.append((float(time), txt))
    return msg_log


def main():
    args = parser.parse_args()

    if not args.real:
        betas[('XKF1_Pitch', 'deg')] *= -1

    # get model parameters
    descriptors = betas.keys()
    coefs = betas.values()

    # read in csv file
    if args.ifname[-4:] != '.csv':
        sys.exit('Dataflash log file no csv format.')
    print('Start reading {}'.format(args.ifname))
    df = dfp.read_csv(args.ifname)
    for f in descriptors:
        if f not in df:
            sys.exit('Needed feature {} for infering'
                     'not found in dataflash log'.format(f))
    if target not in df:
        df[target] = 0.0

    # read in log messages for mode extraction
    log_msg = read_msglog(args.msg)
    if len(log_msg) == 0:
        sys.exit('Unable to extract messages from message log file.')

    # extract fwf mode
    ranges = dfp.get_mode_ranges(log_msg, kind=mode_kind,
                                 mode=flight_mode)
    if len(ranges) == 0:
        sys.exit('Log containing NO forward flight. Infering stopped.')

    print('Start infering {}'.format(args.ifname))
    # apply model
    for r in ranges:
        start = r.start
        end = df.index[df.index.get_loc(r.start_next) - 1]
        df.loc[start:end, target] = (df.loc[start:end, descriptors] *
                                     coefs).sum(axis=1)+constant
        # clip negative current
        df.loc[:, target].clip(0, inplace=True)
        print('Infered range - start: {:.2f}s\tend: {:.2f}s'.format(start, end))

    # write out csv
    ofname = args.ifname[:-4] + '_infer.csv'
    dfp.write_csv(df, ofname)
    print('Infered log written to: {}'.format(ofname))


if __name__ == '__main__':
    main()
    exit(0)
