#/usr/bin/env python
import argparse
from BSLog.DataflashParser import read_from_log, write_csv, interpolate_data
import pandas as pd
import re
import sys
import os
from datetime import datetime


# implement functions
# Function to remove all unwanted features, uses INPLACE functions
# unwanted features are features of a group asked for by -f but not
# part of -f
def trim_data(df, features, featuregroupset, stats):
    cols_to_delete = list()
    for col in df.columns:
        col_name = col[0]
        m = re.search(r'(\w+)_', col_name)
        if m != None:
            group = m.group(1)
            # if the feature is in the featuregroupset but not in the features
            # list, it is not wanted and should be removed
            if group in featuregroupset and not col_name in features:
                cols_to_delete.append(col)
    # clean df
    df.drop(columns=cols_to_delete, inplace=True)
    # clean statistics
    #
    for col in cols_to_delete:
        col_name = col[0]
    # flatliner
        if col_name in stats['FLAT']:
            stats['FLAT'].remove(col_name)
    #zero multiplier
        if col_name in stats['ZERO']:
            stats['ZERO'].remove(col_name)

def get_filename(file):
    start = 0
    for idx, char in enumerate(file):
        if char == '\\' or char == '/':
            start= idx+1
    return(file[start:-4])

# creates a folder if not already existing and returns the path to that folder
def create_folder(foldername):
    folder = os.path.join(os.getcwd(), foldername)
    if not os.path.isdir(folder):
        os.mkdir(folder)
    return folder




### parsing ###
parser = argparse.ArgumentParser(description='Parse log files to csv files, ' +
                    'data is binned to a given resolution and not available data' +
                    ' on certain time stamps is interpolated.')
parser.add_argument('input', nargs='+',
                    help = 'Logfile(s)/folder(s) containing logfile(s) to be parsed.')
parser.add_argument('-r', '--resolution', type=int,
                    help = 'Choose the time precision ' +
                    '(in number of digits after the comma, unit is seconds) data in the' +
                    ' csv file should be presented in, default is 1 (10^-1 s).')
parser.add_argument('-g', '--groups', nargs='+', dest = 'groups',
                    help = 'List of feature groups that should be in the csv file.')
parser.add_argument('-f', '--features', nargs='+', dest = 'features',
                    help = 'List of features that should be in the csv file. Only' +
                    ' the given features of a group are extracted.' +
                    ' Features have to be given in the GROUP_FEATURE, e.g. BAT_Curr, format.')
parser.add_argument('--no_csv',
                    action = 'store_true', default = False,
                    help = 'When given, no csv file will be created, just the parsing_log.')
parser.add_argument('--with_constants', action='store_true', default=False,
                    help='When given, flatline features (features whose value never changes) and'
                    ' features that are multiplied with 0 will' +
                    ' be given in the output csv file. The multiplier 0 will NOT be applied though.')
parser.add_argument('--create_folder_structure', action='store_true', default=False,
                    help='Put csv logs in csv_folder and parsing logs in parsing_logs folder. These' +
                    ' folders will be created if not available already.')
parser.add_argument('--no_interpolation', action='store_true', default=False,
                    help='Do not interpolate datapoints, default is false.')
# change later, mabe make it possible to choose only x flight mode
#parser.add_argument('-o','--onlyforward', action = 'store_true', default = False,
#                help = 'Extract only data with forward flight status')

### MAIN ####
# read in options
args = parser.parse_args()
groups = args.groups
features = args.features
resolution = args.resolution
input = args.input

# fill information with default values:
if resolution == None:
    resolution = 1

# build set to add possible featuregroups
if groups != None:
    groupset = set(groups)
else:
    groupset = set()



# extract features from group
if features != None:
    featureset = set(features)
    featuregroupset = set()
    for f in features:
        m = re.search(r'(\w+)_', f)
        if m == None:
            sys.stderr.write('One or more features given were not in the correct ' +
                            'GROUP_FEATURE format\n')
            exit(1)
        gr = m.group(1)
        groupset.add(gr)
        featuregroupset.add(gr)

if groupset:
    groups = list(groupset)



# iterate over input, finding all files to read
logfiles = list()
for file_or_folder in input:
    if file_or_folder[-4:] == '.log':
        logfiles.append(file_or_folder)
    else:
        #given input is folder
        if os.path.isfile(file_or_folder):
            sys.stderr.write(
            'Invalid input warning: given input {}'.format(file_or_folder) +
            ' is neither a .log file nor a folder and will therefore be ignored\n')
            continue
        fold_files = os.listdir(file_or_folder)
        for file in fold_files:
            if file[-4:] == '.log':
                logfiles.append(os.path.join(file_or_folder, file))

if len(logfiles) == 0:
    sys.stderr.write('No logfiles found, did you maybe give an incorrect folder' +
    ' or a file without the .log ending?\n')
    exit(1)

# read in files
for file in logfiles:
    try:
        ### read in from log and remove unnecessary columns ###
        print('Parsing {}'.format(file))
        df, stats, msg = read_from_log(file, groups, time_precision=resolution,
                                        flatline_remove=False, apply_zeros=False)
        # remove unwanted features fetched
        if features != None:
            trim_data(df, features, featuregroupset, stats)
        # remove flatliners and zeroes
        if not args.with_constants:
            # flatliners and zeroes should be removed
            # first get all features to delete in a set
            feats_to_del = set(stats['FLAT'])
            for feature in stats['ZERO']:
                feats_to_del.add(feature)
            # and then just delete them
            if feats_to_del:
                df.drop(feats_to_del, axis=1, inplace=True, level=0)
        # interpolate data
        if not args.no_interpolation:
            df = interpolate_data(df)

    except Exception as err:
        sys.stderr.write('While reading file {}:\n'.format(file))
        sys.stderr.write(str(err) + '\n')
        exit(1)

    # extract file name without possible folders
    filename = get_filename(file) # filename without the .* ending

    ### Create parsing log header ###
    # create csv folder and add logs there
    if not args.no_csv:
        if args.create_folder_structure:
            csv_folder = create_folder('csv_folder')
            write_csv(df, os.path.join(csv_folder, filename + '.csv'))
        else:
            write_csv(df, file[:-4] + '.csv')

    # start filling header
    outputstring = ''
    outputstring += '##### Header #####\n'
    outputstring += 'Inputile: {}\n'.format(file)
    if args.no_csv:
        outputstring += 'Outputfile: None\n'
    else:
        if args.create_folder_structure:
            outputstring += 'Outputfile: {}\n'.format(os.path.join(csv_folder, filename + '.csv'))
        else:
            outputstring += 'Outputfile: {}\n'.format(
                                                file[:-4] + '.csv')
    curr_time = datetime.now()
    formatted_time = curr_time.strftime('%Y-%m-%d %H:%M:%S %Z')
    outputstring += 'Parsingtime: {}\n'.format(formatted_time)
    if args.no_interpolation:
        outputstring += 'Interpolation: None\n'
    else:
        outputstring += 'Interpolation: linear\n'
    flightdate = None
    if stats['DATE']:
        flightdate = stats['DATE']
    outputstring += 'Flightdate: {}\n'.format(flightdate)
    flightduration = df.index.max() - df.index.min()
    outputstring += 'Flightduration: {}\n'.format(int(flightduration))
    if args.groups == None and args.features == None:
        outputstring += 'Groups given: ALL GROUPS\n'
        outputstring += 'Features given: ALL FEATURES\n'
    else:
        outputstring += 'Groups given: {}\n'.format(args.groups)
        outputstring += 'Features given: {}\n'.format(args.features)
    outputstring += 'Resolution: {}\n'.format(resolution)
    # MISS
    stattmp = None
    if stats['MISS']:
        stattmp = stats['MISS']
    outputstring += 'NotFoundGroups: {}\n'.format(stattmp)
    # ZERO
    if args.with_constants:
        with_constants_prefix = 'Kept in'
    else:
        with_constants_prefix = 'Removed'
    stattmp = None
    if stats['ZERO']:
        stattmp = stats['ZERO']
    outputstring += 'ZeroMultiplierFeatures: {}\n'.format(stattmp)
    outputstring += 'ZeroMultiplierHandling: {}\n'.format(with_constants_prefix)
    # FLAT
    stattmp = None
    if stats['FLAT']:
        stattmp = stats['FLAT']
    outputstring += 'FlatlineFeatures: {}\n'.format(stattmp)
    outputstring += 'FlatlineHandling: {}\n'.format(with_constants_prefix)
    # DUPL
    stattmp = None
    if stats['DUPL']:
        stattmp = stats['DUPL']
    outputstring += 'DuplicateFeatures: {}\n'.format(stattmp)
    outputstring += 'DuplicateHandling: Averaged\n'
    outputstring += '##### Headerend #####\n'

    ### Add msg info ###
    # print header
    outputstring += 'time(s)\tSystem Message\n'
    # print infos
    for tuptup in msg:
        outputstring += '{}\t{}\n'.format(tuptup[0], tuptup[1])


    #### print out outputstring to file###
    if args.create_folder_structure:
        log_folder = create_folder('parsing_logs')
        fp = open(os.path.join(log_folder, filename + '.txt'), 'w')
        fp.write(outputstring)
        fp.close()
        print('File was saved as {}'.format(
                            os.path.join(log_folder, filename + '.txt')))
    else:
        fp = open(file[:-4] + '.txt', 'w')
        fp.write(outputstring)
        fp.close()
        print('File was saved as {}'.format(file[:-4] + '.txt'))
    print('Parsing succesful for file {}'.format(file))
