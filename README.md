# ArduPilot Dataflash Logfile Parser and Battery Current Inferer

[[_TOC_]]

## Dependencies
As both scripts are python scripts, python3 must be installed and used for execution of these scripts. The following python modules have to be installed:
- numpy
- pandas
- importlib_resources
- json5 (0.9.5)


## Script: Dataflash Log Parser <a name="parser"></a>
The script parses an .log file to .csv format, binning and interpolating the data. The name of the file will not change, so parsing flight.log
will create flight.txt (parsing_log) and flight.csv (the log file in csv format).
As a command line tool, different options are available:  
| input | output |
| ------ | ------ |
| dataflash log(s) [`log`] folder(s)| dataflash log [`csv`] |
|| log messages [`txt`] 

### Options
The following options can be choosen:
- -h/--help | Gives Information about all options available
- -g/--groups STRING+ | Option to choose the groups to be extracted, you may give as many groups as you want. Default without neither -g nor -f are all features.
- -f/--features STRING+ | Option to choose the features to be extracted, you may give as many features as you want. Only extract the given features of a group, even if the group is given via -g GROUP. Default without -g or -f are all features.
- -r/--resolution INT | Give the precision to be binned to in 10^-INT s. Default is 1.
- --no_csv | Do not create csv file, just the parsing log (default is with csv)
- --with_constants | Also parse flatline and zero multiplier features. Flatline features are features that constantly have the same value over all time stamps, zero multiplier features are features that should be multiplied by 0. This multiplication with 0 will not be done. Default is not giving these features.
- --create_folder_structure Creates (if not available) a csv_files as well as a parsing_logs folder and saves the parsing_logs and csv_files there. These folders will be created from where the tool is executed. In the default case without this option, files will be saved in the locatio of the `.log` file.
- --no_interpolation Don't interpolate missing data. Default is false.

### Example uses 
1. Parse all `.log` files from directory Documents\log_files, saving parsing_logs and csv_files in same directory.

`py parsing_mn.py Documents\log_files`

2. Parse all .log files from directory Documents\log_files, saving parsing_logs in parsing_logs and csv files in csv_files folder.

`py parsing_mn.py Documents\log_files --create_folder_structure`

3. Extract only the Groups BAT and RATE and the feature ACC1_AccX of the file flight.log  

`py parsing_mn.py flight.log -g BAT RATE -f ACC1_AccX`  

4. Parse all log files of the Documents and Downloads folder with resolution 10^-5 s  

`py parsing_mn.py Documents Downloads -r 5`  

5. Only get the error messages with their time stamp as well as parsing information and no csv file of parsing flight2.log  

`py parsing_mn.py flight2.log --no_csv`

### Output
The output is a .csv file (the parsed log) and a .txt file (the parsing log)

#### The csv log
The csv log contains two header rows. The first row contains the name of the features, the second row contains the unit of these features (UNKNOWN if unknown). The following rows contain the data.  
Example structure:  
time,BAT_Volt,BAT_Curr,BAT_CurrTot  
s,V,A,UNKNOWN  
594.9,25.09839,11.31573,1025.15769  

### The parsing log (.txt)
The parsing log contains Information about the parsing process and contains error messages along their time stamp. 
If Information was not available, None is set (e.g. Flightdate: None).  
Date and time is given in yyyy-mm-dd h:m:s format (e.g. 2021-03-28 16:18:30), time in 24h format.  
Information about features or Groups is given in the \[\'Group1', 'Group2\'\] or \[\'Group1_Feature1\', \'Group1_Feature2\'\] format.  
The structure is the following:  
\#\#\#\#\# Header \#\#\#\#\#  
Inputile: \# The name of the .log file that was parsed  
Outputfile: \# The name of the .csv file that was created  
Parsingtime: \# Date and time parsing was done  
Interpolation: \# The method used for interpolation, currently only linear is available  
Flightdate: \# The date the flight was done  
Flightduration: \# Duration of a flight in seconds, e.g. 51  
Groups given: \# The groups given for parsing, either as list or ALL GROUPS  
Features given: \# The features given for parsing, either as list or ALL FEATURES   
Resolution: \# Resolution x in 10^-x s, e.g. 3  
NotFoundGroups: \# All Groups that were asked for but not found  
ZeroMultiplierFeatures: \# All Features that are multiplied with 0 and therefore left out in the default case, use --with_constants to get these features (without the 0 multiplier applied)  
ZeroMultiplierHandling: \# either Kept in or Removed  
FlatlineFeatures: \# Features that over the whole log file do not change in value  
FlatlineHandling: \# either Kept in or Removed  
DuplicateFeatures: \# Features with different values at the exact same time stamp  
DuplicateHandling: \# The way duplicates were handeled, currently only Averaged is available  
\#\#\#\#\# Headerend \#\#\#\#\#  
time(s) \\tab seperated System Message  
time in s \\tab seperated System message, starting with e.g. \#MSG or \#STAT, depending on from which Group the message was extracted. Example row:    
594.6   #STAT isFlying0_Stage2  

## Script: Battery Current Inferer

This script infere the feature `BAT_Curr` in a `.csv` Dataflash logfile (parsed by the [Dataflash Log Parser](#parser)) for the vehicle Forward Flight (STAT_isFlying = 1, STAT_Stage = 3). It needs the messagelog file from the Dataflash Log Parser to extract the Forward Flight range(s) and will only infer the battery current within this range. The result is written in an extra `.csv` file in the same directory as the input file.

| input | output |
| ------ | ------ |
| dataflash log [`csv`] | dataflash log with infered battery current [`csv`] |
 log messages [`txt`] 

### Model
The model used for inferring is a ordinary least square linear model.

Target value: **BAT_Curr**

Descriptors used for modelling (and so needed in a Dataflash logfile for inferring): 
- **ARSP_Airspeed** (Current airspeed)
- **NTUN_AltErr** (difference between current vehicle height and target heigth)
- **TECS_dsp** (x-axis acceleratin estimate ("delta-speed"))
- **XKF1_Pitch** (Estimated Pitch)

The model has an estimated error of +/- 11 % with respect to the accumulated total of the battery current in one forward flight.

### Training Data
For training and descriptor selection a data set of 75 forward flights (STAT_isFlying = 1, STAT_Stage = 3) was used. The flights were done by an Unmanned Aircraft (Type: F-Series) of the company [Beagle Systems](https://www.beaglesystems.com/). Most of the flights span under one minute and were completed under good weather conditions.

### Example Uses 

**Hint:** Use option `-h` for detailled help: `py fwf_current_inferer.py -h`

1. Inferring the Battery Current feature in a Dataflash logfile `dataflashlog.csv` with corresponding messagelog file `messagelog.txt` from [Dataflash Log Parser](#parser). Infered file will be named `dataflashlog_infer.csv` in the same directory.

`py fwf_current_inferer.py path\to\dataflashlog.csv path\to\messagelog.txt`

**Caution:** If you want to infere the battery current of a real flight Dataflash logfile you need to invoke the `--real` flag. Because the reference system for the `XKF1_Pitch` differs in simulation and a real flight. This requires the negation of the corresponding weight value while inferring.

## Feature and Group Descriptions

In the following tables you see all possible features and groups which could be tracked in a Dataflash log (but sometimes they are not). Features are coded with its corresponding group names: `GROUPNAME_FEATURENAME`.

[Table: Group Descriptions](gdescriptions.md)

[Table: Feature Descriptions](fdescriptions.md)
